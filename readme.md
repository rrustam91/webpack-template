<h1>Webpack Template</h1>

<h2>How to use Webpack Template</h2>

<ol>
	<li><a href="https://gitlab.com/rrustam91/webpack-template/-/archive/master/webpack-template-master.zip">Download</a><strong> Webpack template</strong> from Gitlab</li>
	<li>Install Node Modules: <strong>npm i</strong>;</li>
	<li>Run the template: <strong>npm run start</strong>.</li>
</ol>

 
<h1>Webpack шаблон</h1> 
<h2>Как использовать Webpack шаблон</h2>

<ol>
	<li><a href="https://gitlab.com/rrustam91/webpack-template/-/archive/master/webpack-template-master.zip">Скачайте</a><strong> Webpack шаблон</strong> с Gitlab</li>
    	<li>Установите Node модули и компоненты через комманду: <strong>npm i</strong>;</li>
    	<li>Запустите шаблон, через команду: <strong>npm run start</strong>.</li>
</ol>


